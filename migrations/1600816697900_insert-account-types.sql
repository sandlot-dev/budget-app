-- Up Migration
INSERT INTO account_types (account_type_name)
VALUES ('Checking'),
    ('Savings'),
    ('Credit Card');
-- Down Migration
DELETE FROM account_types;